<?php

use Illuminate\Database\Seeder;

class UpdateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
    	$temp = 25;
    	$humidity = 50;
    	$date = \Carbon\Carbon::now();

        for($i=0;$i<200;$i++) {
        	$insert = new \App\RemoteUpdate();
        	$insert->uuid = 'Uuid-Algo';
        	$insert->temperature = $temp + round(-7-cos($i*0.1)*5,3);
        	$insert->created_at = $date->subMinute(5);
        	$insert->humidity = $humidity;
        	$insert->pumpstatus = 0;
        	$insert->lightstatus = ($date->format('H') > 9 && $date->format('H') < 22) ? 1 : 0;;
        	$insert->water = 0;
        	$insert->save();
        }
    }
}
