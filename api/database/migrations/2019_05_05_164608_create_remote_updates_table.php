<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRemoteUpdatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('remote_updates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('uuid');
            $table->decimal('temperature',4,2);
            $table->decimal('humidity',4,2);
            $table->decimal('water',6,2);
            $table->boolean('pumpstatus');
            $table->boolean('lightstatus');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('remote_updates');
    }
}
