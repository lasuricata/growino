<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">



    <title>Growino Device Status</title>
  </head>
  <body>
<div class="container">



<h6 style="color: #aaa; margin: 20px 0 0 0; padding: 0;">Statistics for</h6>
<h1 style="margin-top: 0; padding-top: 0;">Proto01</h1>


<div class="row">
	
	<div class="col-12 col-sm-6 col-md-3">
		<div class="row" style="margin: 20px 0;">	
			<div class="col-4 text-right">
				<i style="color: #FFED1C;" class="fas fa-lightbulb fa-4x"></i>
			</div>
			<div class="col-8">
			
			<small>Luz encendida</small>
			<h2>
				8h 12m
			</h2>
			</div>

		</div>
	</div>

	<div class="col-12 col-sm-6 col-md-3">
		<div class="row" style="margin: 20px 0;">	
			<div class="col-4 text-right">
				<i style="color: #55C3DC;" class="fas fa-water fa-4x"></i>
			</div>
			<div class="col-8">
			
			<small>Ultimo Riego</small>
			<h2>
				{{ $last->water }}ml
			</h2>
			</div>

		</div>
	</div>
	<div class="col-12 col-sm-6 col-md-3">
		<div class="row" style="margin: 20px 0;">
	<div class="col-4 text-right">
		<i style="color: #CF4647" class="fas fa-thermometer-half fa-4x"></i>
	</div>
	<div class="col-8">
		<small>Temperatura Actual</small>
		<h2>
			{{ round($last->temperature,1) }}&deg;C
		</h2>
	</div>
</div>
</div>
	<div class="col-12 col-sm-6 col-md-3">

		<div class="row" style="margin: 20px 0;">

	<div class="col-4 text-right">
		<i style="color: #A7C5BD" class="fas fa-tint fa-4x"></i>
	</div>
	<div class="col-8">
		<small>Humedad Actual</small>
		<h2>
			{{ round($last->humidity,1) }}%
		</h2>
	</div>
</div>

</div>

</div>
</div>
<div style="height: 400px; overflow: hidden">
	{!! $tempchart->container() !!}
</div>


	</div>	



</div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>



{!! $tempchart->script() !!}

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>




<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


