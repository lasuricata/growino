<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Charts\TemperatureStat;

class DeviceController extends Controller
{
    
	public function stats($uuid) {


		$status = \App\RemoteUpdate::selectRaw('max(lightstatus) lightstatus, AVG(temperature) temperature, AVG(humidity) humidity, DATE_FORMAT(created_at, \'%d-%m %H:00\') custom_date, MIN(created_at) created_at')->where('uuid',$uuid)->groupBy('custom_date')->orderBy('custom_date','DESC')->limit(24)->get();

		$last = \App\RemoteUpdate::where('uuid', $uuid)->orderBy('id', 'DESC')->first();
		
		if(!count($status)) {
		 	abort(404);
		}

		$chart = new TemperatureStat;

		foreach($status as $state) {
			$temp_labels[] = $state->custom_date;
			$temp_keys[] = $state->custom_date;
			$temp_dataset[] = $state->temperature;
			$hum_dataset[] = $state->humidity;
			$lightstatus[] = $state->lightstatus*100;
			$ideal_temp[] = 25;
			$ideal_hum[] = 50;
		}

		//templast = \App\RemoteUpdate::where('uuid',$uuid)->limit(30)->get();

		/*foreach($templast as $data) {
			
			$ideal_temp[] = 25;
			$temp2_keys[] = $data->created_at->format("H:i");
			$temp2_dataset[] = $data->temperature;

		}*/

		$tempchart = new TemperatureStat;

		$chart->labels($temp_keys);
	
		#$tempchart->dataset('Temperature', 'line', $temp2_dataset)->color('#CF4647');


		$chart->dataset('Temperatura', 'line', array_reverse($temp_dataset))->backgroundColor(false)->color('#CF4647');
		$chart->dataset('Temp. Ideal', 'line', array_reverse($ideal_temp))->color('#CF4647')->backgroundColor(false)->dashed([5])->options(['pointRadius'=>'0px'])	;

		$chart->dataset('Humedad Relativa', 'line', array_reverse($hum_dataset))->backgroundColor(false)->color('#A7C5BD');
		$chart->dataset('Humedad Ideal', 'line', array_reverse($ideal_hum))->backgroundColor(false)->color('#A7C5BD')->options(['pointRadius'=>0])->dashed([5]);

		$chart->dataset('Luz', 'line', array_reverse($lightstatus))->color('rgba(250,190,50,.0)')->backgroundColor('rgba(250,190,50,.2)')->dashed([1])->options(['pointRadius'=>0,'steppedLine'=>'before']);


		$chart->options([
			'tooltips'=>['mode'=>'index'], 
			'scales'=>[

				'xAxes'=>[[  ]], 

				'yAxes' => [
					[
					'scaleLabel'=>['display'=>false, 'fontSize'=>10]
					]
				]
			]

		])->minimalist(true);

		return view('device_stat', ['last'=>$last, 'uuid'=>$uuid, 'chart' => $chart, 'tempchart'=>$chart]);
	}

	public function list() {
		$uuids = \App\RemoteUpdate::selectRaw('DISTINCT uuid')->get();

		return view('device_list', ['list'=>$uuids]);
	}
}
