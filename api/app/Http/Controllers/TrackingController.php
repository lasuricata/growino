<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TrackingController extends Controller
{
    public function register(Request $request) {
    	list($uuid, $timestamp, $temperature, $humidity, $watercount, $pumpstatus, $lightstatus) = explode(',',$request->getContent());

    	$update = new \App\RemoteUpdate();

    	$update->uuid = $uuid;
    	$update->temperature = $temperature;
    	$update->humidity = $humidity;
    	$update->water = $watercount;
    	$update->pumpstatus = $pumpstatus;
    	$update->lightstatus = $lightstatus;

    	$update->save();
    	
    	
    }
}
