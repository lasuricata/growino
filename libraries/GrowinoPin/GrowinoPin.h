/*
  GrowinoPin.h
*/
#include <Time.h>
#include <EEPROM.h>


#ifndef GrowinoPin_h
#define GrowinoPin_h

#if ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#define MODE_TIMER 0;
#define MODE_TEMP 1;
#define MODE_INTERVAL 2;

typedef struct {
  uint8_t startHour;
  uint8_t startMinute;
  uint8_t endHour;
  uint8_t endMinute;
} timerSettings;

typedef struct {
  uint8_t maxTemp;
  uint8_t minTemp;
  uint8_t isPWM;
} tempSettings;

typedef struct {
  uint16_t durationSeconds; 
  uint16_t intervalSeconds;
  uint32_t lastRun;
} intervalSettings;

class GrowinoPin 
{
  public:
    GrowinoPin(uint8_t outPin, uint16_t memoryAddr);

    void check(tmElements_t time, float temp, float hum);
    void setup();
    bool isOn();
    void blink();
    uint8_t _pinStatus = LOW;
    uint8_t _toggleMode;
    uint8_t _outPin;
    static const uint8_t timerMode = MODE_TIMER;
    static const uint8_t tempMode = MODE_TEMP;
    static const uint8_t intervalMode = MODE_INTERVAL;
    uint8_t _memoryAddr;
    timerSettings _timerSettings;
    timerSettings _timerDefaults = { 6, 0, 18, 0 };
    tempSettings _tempSettings;
    intervalSettings _intervalSettings;

  private:
    
    
    uint8_t _memoryConfigAddr;

    void _initPin();
    void  debug(char* text);
    void _readConfig();
    void _saveConfig();


};



#endif