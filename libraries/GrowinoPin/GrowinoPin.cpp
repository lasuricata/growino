/*
  GrowinoPin.cpp
*/
#include "Arduino.h"
#include "GrowinoPin.h"

GrowinoPin::GrowinoPin(uint8_t outPin, uint16_t memoryAddr) {
    _outPin = outPin;
    _memoryAddr = memoryAddr; 
}


void GrowinoPin::setup() {
  pinMode(_outPin, OUTPUT);
  digitalWrite(_outPin, _pinStatus);
  _readConfig();
}

bool GrowinoPin::isOn() {
    return _pinStatus;
}

void GrowinoPin::check(tmElements_t time, float temp, float hum) {
  uint8_t _newStatus = LOW;
  switch(_toggleMode) {
    case timerMode: 
      if(time.Hour > _timerSettings.startHour || (time.Hour >= _timerSettings.startHour && time.Minute >= _timerSettings.startMinute)) {
          if(time.Hour < _timerSettings.endHour || (time.Hour == _timerSettings.endHour && time.Minute <= _timerSettings.endMinute)) {
              _newStatus = HIGH;
          }
        } else {
          if(_timerSettings.startHour > _timerSettings.endHour) {
              if(time.Hour < _timerSettings.endHour || (time.Hour <= _timerSettings.endHour && time.Minute < _timerSettings.endMinute)) {
                _newStatus = HIGH;
              }
          }
        }

        if(_newStatus!=_pinStatus) {
          _pinStatus = _newStatus;
          digitalWrite(_outPin, _pinStatus);
        }
      break;
      case tempMode:
      if(temp > _tempSettings.maxTemp) {
        _newStatus = HIGH;
      }
      break;
      case intervalMode:
      unsigned long runDuring = _intervalSettings.durationSeconds*1000L;
      unsigned long runOn = _intervalSettings.lastRun + runDuring + _intervalSettings.intervalSeconds*1000L;
      if(millis() > runOn) {
        _intervalSettings.lastRun = millis();
        if(millis() < _intervalSettings.lastRun + runDuring) {
            _newStatus = HIGH;
        }
      } else {
        if( millis() > _intervalSettings.lastRun && millis() < _intervalSettings.lastRun + runDuring ) {
           _newStatus = HIGH; 
        }
      }
      break;
  }

  if(_newStatus!=_pinStatus) {
    _pinStatus = _newStatus;
    digitalWrite(_outPin, _pinStatus);
  }
}

void GrowinoPin::_readConfig() {
  _toggleMode = EEPROM.read(_memoryAddr);
  switch(_toggleMode) {
    case timerMode:
          EEPROM.get(_memoryAddr+1, _timerSettings);
          if(_timerSettings.startHour > 23 || 
            _timerSettings.startMinute > 60 || 
            _timerSettings.endHour > 23 || 
            _timerSettings.endMinute > 60) {
            EEPROM.put(_memoryAddr+1,_timerDefaults);
            _timerSettings = _timerDefaults;
          }
        break;
    case tempMode:
        EEPROM.get(_memoryAddr+1, _tempSettings);
       break;
    case intervalMode:
        EEPROM.get(_memoryAddr+1, _intervalSettings);
       break;
    default:
      blink();blink();blink();
      _toggleMode = timerMode;
      EEPROM.write(_memoryAddr, timerMode);

      _timerSettings = _timerDefaults;
      EEPROM.put(_memoryAddr+1,_timerDefaults);
      


      break;
  }
}

void GrowinoPin::blink() {
  digitalWrite(_outPin, LOW);
  digitalWrite(_outPin, HIGH);
  delay(200);
  digitalWrite(_outPin, LOW);

}
void GrowinoPin::_saveConfig() {

}

void GrowinoPin::debug(char* text) {
  Serial.print("Port");
  Serial.print(_outPin);
  Serial.print(": ");
  Serial.println(text);
}