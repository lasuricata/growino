#include <HTTPClient.h>

TimedAction uploadStatusTimer = TimedAction(5000, uploadStatus);

void setup_wifi() {
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  u8g2.setFont(u8g2_font_lastapprenticebold_tr);

  uint8_t text_x = 128/2 - u8g2.getStrWidth(ssid)/2 ;
  u8g2.drawXBM(58,5, wifilogo_width, wifilogo_height, wifilogo_bits);
  u8g2.drawStr(text_x,52,ssid); 
   
  while (WiFi.status() != WL_CONNECTED) {
    u8g2.setFont(u8g2_font_tom_thumb_4x6_t_all);
    uint8_t status_x = 128/2 - u8g2.getStrWidth("Conectando")/2;
    u8g2.drawStr(status_x,36,"Conectando");
    u8g2.sendBuffer();
    delay(500);
  }

  u8g2.clearBuffer();
  
}

char datita[255]      = "";

void uploadStatus() {
  
  HTTPClient http;
  sprintf(datita, "%s,%d,%.1f,%.1f,%.2f,%d,%d", uuid,timestamp,temperature, humidity, water.waterCounter, water.pump_state, light.pin_state);
  
  http.begin("http://api.growino.com/"); //Specify the URL
  int httpCode = http.POST(datita);
  if (httpCode > 0) {
      String payload = http.getString();
      Serial.println(httpCode);
      Serial.println(payload);
    }
  else {
    Serial.println("Error on HTTP request");
  }

  http.end(); //Free the resources
    
}
void loop_network() {
    uploadStatusTimer.check();
  
}
