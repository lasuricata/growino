#include "DHTesp.h"
#define dhtPin 13

DHTesp dht;

void refreshThermal() {
   humidity = dht.getHumidity();
   temperature = dht.getTemperature();
}

TimedAction refreshThermalAction = TimedAction(5000, refreshThermal);

void thermal_setup()
{
  dht.setup(dhtPin, DHTesp::DHT22);
}

void thermal_loop()
{
   refreshThermalAction.check();
}
