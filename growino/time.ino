#include "time.h"

const char* ntpServer = "pool.ntp.org";
const long  gmtOffset_sec = 3600 * -3;
const int   daylightOffset_sec = 0;

void rtc_setup() {
 // El RTC esta mal
  if (!rtc.begin() || rtc.lostPower()) {
    u8g2.clearBuffer();
    u8g2.drawXBM(0,0, message_width, message_height, message_bits);
    u8g2.drawStr(20,20, "Ajustando Reloj");
    u8g2.sendBuffer();
  
    configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);
    getLocalTime(&timeinfo);

    
    // Ajustamos el RTC usando la fecha que obtuvimos por NTP
    rtc.adjust(DateTime(timeinfo.tm_year+1900, timeinfo.tm_mon+1, timeinfo.tm_mday, timeinfo.tm_hour, timeinfo.tm_min, timeinfo.tm_sec));
  }
}
