
TimedAction tickDisplay = TimedAction(3000, cycleScreen);
uint8_t currentDisplay = 0;
uint8_t maxDisplay = 5;

long lastTouch;

void cycleScreen() {
  if(millis()-lastTouch > 1000 * 10) {
    displayNext();
  }
}
void IRAM_ATTR next() {
  if(millis()-lastTouch > 150) {
     displayNext();
     lastTouch = millis();
  }
    
}

void setup_display() {
  delay(100);
  u8g2.begin();
  
  u8g2.clearBuffer();
  u8g2.drawXBM(display_width / 2 - greenlady_width /2, display_height / 2 - greenlady_height / 2, greenlady_width, greenlady_height, greenlady_logo);
  u8g2.sendBuffer();
  delay(8000);
  u8g2.clearBuffer();
  pinMode(25, INPUT_PULLUP);
  attachInterrupt(25, next, FALLING);
}

void displayNext() {
  currentDisplay++;
  if(currentDisplay>maxDisplay) {
    currentDisplay=0;
  }
}




void loop_display() {
  tickDisplay.check();
  if(millis()-lastTouch>1000*80) {
    u8g2.clearBuffer();
    u8g2.sendBuffer();
    return;
  }
  

  switch(currentDisplay) {
    case 0:
      display_clock_status();
      break;
    case 1:
      display_plant_status();
      break;
    case 2:
      display_thermal_status();  
      break;
    case 3:
      display_wifi_status(); 
      break;
    case 4:
      display_light_status();
      break;
    case 5:
      display_water_status();
      break;
  }
}

void display_wifi_status() {
  u8g2.clearBuffer();
  u8g2.setFontPosTop();
  u8g2.drawXBM(0,PADDING, wifilogo_width, wifilogo_height, wifilogo_bits);
  u8g2.setFont(u8g2_font_tom_thumb_4x6_t_all);
  u8g2.drawStr(wifilogo_width+PADDING,PADDING,"CONECTADO"); 
  u8g2.setFont(u8g2_font_lastapprenticebold_tr);
  u8g2.drawStr(wifilogo_width+PADDING,PADDING+6,ssid); 
  u8g2.setFont(u8g2_font_beanstalk_mel_tr);
  sprintf(oledTxt, "%d.%d.%d.%d", WiFi.localIP()[0], WiFi.localIP()[1], WiFi.localIP()[2], WiFi.localIP()[3]);
  u8g2.drawStr(wifilogo_width+PADDING,32, oledTxt);  
  u8g2.sendBuffer();
}

void display_plant_status() {
  u8g2.clearBuffer();
  u8g2.setFontPosTop();
  u8g2.drawXBM(3,PADDING, plant_width, plant_height, plant_bits);
  u8g2.setFont(u8g2_font_tom_thumb_4x6_t_all);
  u8g2.drawStr(wifilogo_width+PADDING,PADDING-1,"ETAPA"); 
  u8g2.setFont(u8g2_font_lastapprenticebold_tr);
  u8g2.drawStr(wifilogo_width+PADDING,PADDING+6,"Pre-floracion"); 
  u8g2.setFont(u8g2_font_tom_thumb_4x6_t_all);
  u8g2.drawStr(wifilogo_width+PADDING,31,"Tiempo");
  u8g2.drawStr(wifilogo_width+PADDING+56,31,"Restan");
  u8g2.setFont(u8g2_font_lastapprenticebold_tr);
  sprintf(oledTxt, "%d", ((timestamp - plant.start) / (int) DAYS) + 1);
  u8g2.drawStr(wifilogo_width+PADDING,38, oledTxt);  
  u8g2.setFont(u8g2_font_beanstalk_mel_tr);
  u8g2.drawStr(wifilogo_width+PADDING+PADDING+u8g2.getStrWidth(oledTxt),38, "dias");  
  u8g2.drawXBM(wifilogo_width+PADDING+56,41, infinity_width, infinity_height, infinity_bits);
  
  u8g2.sendBuffer();

}


void display_water_status() {
  u8g2.clearBuffer();
  u8g2.setFontPosTop();
  
  u8g2.setFont(u8g2_font_tom_thumb_4x6_t_all);
  u8g2.drawStr(wifilogo_width+PADDING,PADDING-1,"BOMBA DE AGUA"); 
  u8g2.setFont(u8g2_font_lastapprenticebold_tr);
  if(water.pump_state) {
    u8g2.drawXBM(3,PADDING, icon_dropfull_width, icon_dropfull_height, icon_dropfull_bits);
    u8g2.drawStr(wifilogo_width+PADDING,PADDING+6,"Activado"); 
  } else {
    u8g2.drawXBM(3,PADDING, dropicon_width, dropicon_height, dropicon_bits);
    u8g2.drawStr(wifilogo_width+PADDING,PADDING+6,"En espera"); 
  }

  if(water.pump_state) {
    u8g2.setFont(u8g2_font_tom_thumb_4x6_t_all);
    u8g2.drawStr(wifilogo_width+PADDING,31,"Actual");
    u8g2.drawStr(wifilogo_width+PADDING+56,31,"/riego");
    u8g2.setFont(u8g2_font_beanstalk_mel_tr);
    sprintf(oledTxt, "%.2fml", water.waterCounter);
  } else {
    u8g2.setFont(u8g2_font_tom_thumb_4x6_t_all);
    u8g2.drawStr(wifilogo_width+PADDING,31,"Proximo");
    u8g2.drawStr(wifilogo_width+PADDING+56,31,"/riego");
    u8g2.setFont(u8g2_font_beanstalk_mel_tr);
    long nw = water.getNextWater(plant) - timestamp;
    Serial.print("Nextwater");
    Serial.println(nw);
    sprintf(oledTxt, "%d:%02d:%02d", nw / (int) HOURS, nw % HOURS / 60, nw % 60);
  }
  
  u8g2.drawStr(wifilogo_width+PADDING,38, oledTxt);  
  sprintf(oledTxt, "%dml", plant.waterAmount);
  u8g2.drawStr(wifilogo_width+PADDING+50,38, oledTxt);  
  
  u8g2.sendBuffer();

}

void display_clock_status() {
  u8g2.clearBuffer();
  u8g2.setFontPosTop();
  u8g2.drawXBM(3,PADDING, clock_width, clock_height, clock_bits);
  u8g2.setFont(u8g2_font_tom_thumb_4x6_t_all);
  u8g2.drawStr(wifilogo_width+PADDING,PADDING-1,"HORA"); 
  u8g2.drawStr(wifilogo_width+PADDING,PADDING-1+35,"FECHA"); 
  u8g2.setFont(u8g2_font_lastapprenticebold_tr);
  sprintf(oledTxt, "%02d:%02d:%02d", now.hour(), now.minute(), now.second());
  u8g2.drawStr(wifilogo_width+PADDING,PADDING+7,oledTxt); 
  sprintf(oledTxt, "%02d/%02d/%04d", now.day(), now.month(), now.year());
  u8g2.drawStr(wifilogo_width+PADDING,PADDING+7+35,oledTxt); 
  u8g2.sendBuffer();

}


void display_thermal_status() {
  u8g2.clearBuffer();
  u8g2.setFontPosTop();
  u8g2.drawXBM(3,PADDING, thermo_icon_width, thermo_icon_height, thermo_icon_bits);
  u8g2.setFont(u8g2_font_tom_thumb_4x6_t_all);
  u8g2.drawStr(wifilogo_width+PADDING,PADDING-1,"TEMP."); 
  u8g2.drawStr(wifilogo_width+PADDING+50,PADDING-1,"HUMEDAD"); 
  u8g2.drawStr(wifilogo_width+PADDING,PADDING+28,"ENTRADA");
  u8g2.drawStr(wifilogo_width+PADDING+50,PADDING+28,"SALIDA");
  u8g2.setFont(u8g2_font_lastapprenticebold_tr);
  sprintf(oledTxt, "%.1f C", temperature);
  u8g2.drawStr(wifilogo_width+PADDING,PADDING+7,oledTxt); 
  sprintf(oledTxt, "%.1frH", humidity);
  u8g2.drawStr(wifilogo_width+PADDING+50,PADDING+7,oledTxt); 
  u8g2.drawStr(wifilogo_width+PADDING,PADDING+28+7,"SI"); 
  u8g2.drawStr(wifilogo_width+PADDING+50,PADDING+28+7,"SI"); 
  u8g2.sendBuffer();

}

void display_light_status() {
  u8g2.clearBuffer();
  u8g2.setFontPosTop();
  
  u8g2.setFont(u8g2_font_tom_thumb_4x6_t_all);
  u8g2.drawStr(wifilogo_width+PADDING,PADDING-1,"CONTROL DE LUZ"); 
  u8g2.setFont(u8g2_font_lastapprenticebold_tr);

  if(light.pin_state) {
    u8g2.drawStr(wifilogo_width+PADDING,PADDING+6,"Activado"); 
    u8g2.drawXBM(0,PADDING, icon_sun_width, icon_sun_height, icon_sun_bits);    
    u8g2.setFont(u8g2_font_tom_thumb_4x6_t_all);
    u8g2.drawStr(wifilogo_width+PADDING,31,"RESTAN");
    long rl = light.remaining - timestamp; 
    sprintf(oledTxt, "%d:%02d:%02d", rl / (int) HOURS, rl % HOURS / 60, rl % 60);
  } else {
    u8g2.drawStr(wifilogo_width+PADDING,PADDING+6,"Desactivado"); 
    u8g2.drawXBM(0,PADDING, moon_width, moon_height, moon_bits);
    long ls = plant.lightStart; 
    sprintf(oledTxt, "%d:%02d:%02d", ls / (int) HOURS, ls % HOURS / 60, ls % 60);
    u8g2.setFont(u8g2_font_tom_thumb_4x6_t_all);
    u8g2.drawStr(wifilogo_width+PADDING,31,"ENCENDIDO");
  }
  
  
  
  //u8g2.drawStr(wifilogo_width+PADDING+60,31,"/DIA");
  
  u8g2.setFont(u8g2_font_beanstalk_mel_tr);
  u8g2.drawStr(wifilogo_width+PADDING,38, oledTxt);  
  
  //sprintf(oledTxt, "%02d:%02d", 22,0);
  //u8g2.drawStr(wifilogo_width+PADDING+50,38, oledTxt);  
  
  u8g2.sendBuffer();
}
