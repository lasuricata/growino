volatile long pulseCount=0;

void ICACHE_RAM_ATTR handlePulse() {
  pulseCount++;
}

void water_loop() {
  while(pulseCount>0) {
    water.handlePulse();
    pulseCount--;
  }
}

void setup_water() {
  digitalWrite(WATER_PUMP_PIN, LOW);
  pinMode(WATER_PUMP_PIN, OUTPUT);
  pinMode(WATER_METER_PIN, INPUT);
  pinMode(WATER_METER_PIN, INPUT_PULLUP);

  attachInterrupt(WATER_METER_PIN, handlePulse, RISING);
}
