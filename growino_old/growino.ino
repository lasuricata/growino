#include <EEPROM.h>

#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h>
#include <Time.h>
#include <Wire.h>
#include <DHT.h>
#include <TimedAction.h>
#include <DS1307RTC.h>
#include <GrowinoPin.h>
#define LCDWIDTH = 84;
#define LCDWIDTH = 48;

/* Display Setup */
Adafruit_PCD8544 display = Adafruit_PCD8544(13, 12, 11, 10, 9);

/* Temperature sensor setup */
DHT dht(A0 , DHT11);
TimedAction refreshTempAction = TimedAction(2000, refreshTemperature);

/* Output setup */
GrowinoPin channel_a(7,0);
GrowinoPin channel_b(8,64);
GrowinoPin channel_c(0,128);
GrowinoPin channel_d(1,192);

tmElements_t currentTime;
float currentTemperature;
float currentHumidity;

void setup() {
  channel_a.setup();  
  channel_b.setup();
  channel_c.setup();
  channel_d.setup();

  EEPROM.write(0,0);
  timerSettings t_a { 9,00,15,00 };
  EEPROM.put(1,t_a);
  
  EEPROM.write(64,0);
  timerSettings t_b { 6,00,18,00 };
  EEPROM.put(65,t_b);

  EEPROM.write(128,1);
  tempSettings t_c { 26,10,0 };
  EEPROM.put(129,t_c );

  EEPROM.write(192,2);
  intervalSettings t_d { 1, 5, 0 };
  EEPROM.put(193,t_d);
  
  
  setup_rtc();
  setup_temp();
  setup_display();
}

void setup_display() {
  display.begin();
  display.display();
  display.setRotation(90);
  display.setContrast(50);
}

void setup_rtc() {
  setSyncProvider(RTC.get);
  if(timeStatus()!= timeSet) {
    if (getDate(__DATE__) && getTime(__TIME__)) {
      if(RTC.write(currentTime)) {
           setSyncProvider(RTC.get);
      }
    }
  }
}

void setup_temp() {
  dht.begin();
}

void loop() {
  refreshTempAction.check();
  loop_time();
  loop_temp();
  channel_a.check(currentTime, currentTemperature, currentHumidity);
  channel_b.check(currentTime, currentTemperature, currentHumidity);
  channel_c.check(currentTime, currentTemperature, currentHumidity);
  channel_d.check(currentTime, currentTemperature, currentHumidity);
  loop_display();
}

void loop_display() {
  display.clearDisplay();
  displayClock();
  displayTemperature();
  displayStatus(channel_a,0);
  displayStatus(channel_b,1);
  displayStatus(channel_c,2);
  displayStatus(channel_d,3);
  display.setCursor(0,36);
  display.print(channel_d._intervalSettings.lastRun);
  display.setCursor(0,42);
  display.print(millis());
  display.display();  
}

void loop_time() {
  /* Forzamos el paso del tiempo para poder probar */
  currentTime.Second = currentTime.Second +60;
  if(currentTime.Second >= 60) {
    currentTime.Minute++;
    currentTime.Second = 0;
  }
  
  if(currentTime.Minute >= 60) {
    currentTime.Hour++;
    currentTime.Minute = 0;
  }

  if(currentTime.Hour==24) {
    currentTime.Hour = 0;
  }
}

void loop_temp() {
  
}

