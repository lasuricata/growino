void displayStatus(GrowinoPin pin, uint8_t id) {
  uint8_t startX = 44 + (id*10) + 1;
  if(pin.isOn()) {
    display.fillRect(startX,0,9,10,1);  
  } else {
    display.drawRect(startX,0,9,10,1);
  }
  display.setCursor(startX,11);
  display.print(pin._toggleMode);
  display.setCursor(startX,28);
  display.print(pin._outPin);
}

void displayClock() {
  display.setCursor(0,0);
  display.setTextSize(1);
  display.print(currentTime.Hour);
  display.print(":");
  display.print(currentTime.Minute); 
}

void displayTemperature() {
  display.setCursor(0,20);
  display.print(currentTemperature);
}

