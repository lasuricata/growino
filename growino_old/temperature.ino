void refreshTemperature() {
  currentHumidity = dht.readHumidity();
  currentTemperature = dht.readTemperature();
}
