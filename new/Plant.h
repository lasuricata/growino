#if ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#ifndef Plant_h
#define Plant_h

typedef enum { VEGETATIVE, PRE_FLOWER, FLOWERING } t_currentPeriod;
typedef enum { SEASON, FIXED } t_currentMode;

typedef struct {
  long start;

  t_currentPeriod currentPeriod;
  t_currentMode currentMode;
  
  /* Lighting */
  int vegetativePeriod;
  int vegetativeHours;
  int floweringPeriod;
  int floweringHours;
  int periodTransition;

  long lightStart;
  bool lightReversed;

  /* Watering */
  int waterAmount;
  long waterInterval;
  long waterLast;

  /* Thermal */
  int tempTarget;
  int tempAmplitude;
  int humidityTarget;
  int humidityAmplitude;


  bool changed;

} Plant;

#endif