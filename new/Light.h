#if ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#include "Plant.h"

#ifndef Light_h
#define Light_h

typedef struct {
  int start;                // Time of the day when the light turns on.
  int runtime;

  // Transition Information
  bool transitionEnabled;   
  long transitionStart;     // When the last/current transition has started
  long transitionDuration;   // How many seconds the trasition between periods will last
  int transitionRuntime;    // Destination runtime

} LightConfig;

class Light {
  LightConfig config;

  private:
    int out_pin;

  public:
    int remaining;
    bool pin_state;
    Light(int outPin);
    void tick(long actualTime, Plant &plant);
};

#endif