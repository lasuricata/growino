#if ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#include "Plant.h"

#ifndef Water_h
#define Water_h

typedef struct {
  int pulseDebounce;
  float pulseAmount;
} WaterConfig;

class Water {
  public:
    Water(int pumpPin, int meterPin);
    void tick(long actualTime, Plant &plant);
    void handlePulse();
    float waterCounter;
    float totalCounter;
    bool pump_state;
    int getNextWater(Plant &plant);

  private:
    void start();
    void end();
    long last_pulse;
    long last_run;

    int pump_pin;
    int pulse_pin;

    WaterConfig config;
};


#endif