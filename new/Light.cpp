
#include "Light.h"
#include "Compat.h"

void Light::tick(long actualTime, Plant &plant) {
    int elapsedSeconds = (actualTime % 86400);
    /* Plant Tick Section */
    int plantAge = (actualTime - plant.start) / 86400;
    if(plantAge <= plant.vegetativePeriod) {
        config.runtime = plant.vegetativeHours * HOURS;
        plant.currentPeriod = VEGETATIVE;
    } else {
        if(plant.currentPeriod == VEGETATIVE) {
            if(plantAge < plant.vegetativePeriod + plant.periodTransition) {
                config.runtime = plant.vegetativeHours * HOURS;
                plant.currentPeriod = PRE_FLOWER;
                config.transitionEnabled = true;
                config.transitionStart = plant.start + (plant.vegetativePeriod * 86400);
                config.transitionDuration = plant.periodTransition * 86400;
                config.transitionRuntime = plant.floweringHours * HOURS;
            } else {
                config.runtime = plant.floweringHours * HOURS;
                plant.currentPeriod = FLOWERING;
            }
        }
    }

    /* End plant tick section */

    long realStart = actualTime - elapsedSeconds + plant.lightStart;
    long realEnd = actualTime - elapsedSeconds + plant.lightStart + config.runtime;

    if(plant.lightStart + config.runtime > 86400 && elapsedSeconds < (plant.lightStart + config.runtime) % 86400) {
        realEnd = realStart - 86400 + config.runtime;
        realStart = realStart - 86400;
    }

    if(config.transitionEnabled) {
        if(actualTime > config.transitionStart && actualTime < config.transitionStart + config.transitionDuration) {
            int daysTotal = config.transitionDuration / 86400;
            int daysElapsed = ( actualTime - config.transitionStart ) / 86400;
            
            float dailyMovement;
            if(config.transitionRuntime > config.runtime) {
                dailyMovement = (config.transitionRuntime - config.runtime) / daysTotal;
                realEnd = realEnd + (dailyMovement * daysElapsed);
            } else {
                dailyMovement = (config.runtime - config.transitionRuntime) / daysTotal;
                realEnd = realEnd - (dailyMovement * daysElapsed);
            }
        } else {
            if(actualTime > config.transitionStart + config.transitionDuration) {
                config.transitionEnabled = false;
                plant.currentPeriod = FLOWERING;
                config.runtime = plant.floweringHours * HOURS;
            }
        }
    }

    int periodAge;

    switch(plant.currentPeriod) {
        case 0:
            periodAge = plantAge;
            break;
        case 1:
            periodAge = plantAge - plant.vegetativePeriod;
            break;
        case 2:
            periodAge = plantAge - plant.vegetativePeriod - plant.periodTransition;
    }
    
    bool new_status = false;

    remaining = realEnd;
    
    if(actualTime > realStart && actualTime < realEnd) {
        new_status = true;
    } else {
        new_status = false;
    }

    pin_state = new_status;
    digitalWrite(out_pin, !pin_state);
}  

Light::Light(int outPin) {
    out_pin = outPin;
    pinMode(out_pin, OUTPUT);
}