#include "UserInterface.h"
#include "Plant.h"


UserInterface::UserInterface(Plant plant) {

	optionItem plantMenu[] = {
		{ "Vegetativo", plant.vegetativePeriod, "d" },
		{ "Luz diaria", plant.vegetativePeriod, "h" },
		{ "Floracion", plant.floweringPeriod, "d" },
		{ "Luz Diaria", plant.floweringHours, "h" },
		{ "Transicion", plant.periodTransition, "h" }
	};

	optionItem lightMenu[] = {
		{ "Inicio", plant.lightStart, "h" }
	};

	optionItem waterMenu[] = {
		{ "Cantidad", plant.waterAmount, "ml" },
		{ "Intervalo", plant.waterInterval, "h" }
	};

	optionItem thermalMenu[] = {
		{ "Ideal", plant.tempTarget, "C" },
		{ "Amplitud", plant.tempAmplitude, "C" }

	};

	optionItem wifiMenu[] {
		{ "Bluetooth", plant.tempTarget, "BIN" },
		{ "WiFi", plant.tempAmplitude, "C" },
		{ "SSID", plant.tempAmplitude, "C" },
		{ "GrowinoNet", plant.tempAmplitude, "C" }
	};

	main_menu = {
		{1, "Planta", MODE_SUB, plantMenu},
		{2, "Iluminacio", MODE_SUB, lightMenu },
		{3, "Riego",MODE_SUB, waterMenu},
		{4, "Temperatur", MODE_SUB, thermalMenu }
	};

}

double UserInterface::easeInOutSine( double t ) {
	return 0.5 * (1 + sin( 3.1415926 * (t - 0.5) ) );
}

void UserInterface::mainMenuTransition(int source, int destination) {
	long transitionStart = millis();
	int itransitionDelay = 400;

} 

void UserInterface::handleAction(int action) {
	switch(action) {
		case ACTION_UP:
			lastpointer = pointer;
			pointer++;

			switch(mode) {
				case MODE_MAIN:
					mainMenuTransition(pointer, lastpointer);
					break;
			}

			break;
		case ACTION_DOWN:
			lastpointer = pointer;
			pointer--;

			switch(mode) {
				case MODE_MAIN:
					mainMenuTransition(pointer, lastpointer);
					break;
			}

			break;
		case ACTION_OK:

			switch(mode) {
				case MODE_MAIN:
					mode = MODE_SUB;
					current_menu = main_menu[pointer];
					break;
			}

			break;
		case ACTION_BACK:
			if(mode == MODE_SUB) {
				pointer = 0;
				mode = MODE_MAIN;
			}
			break;
	}
}

void UserInterface::display() {
}