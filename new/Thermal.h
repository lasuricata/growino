
#if ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#include "Plant.h"

#ifndef Thermal_h
#define Thermal_h

typedef struct {
    
    int targetTemp;
    int threshold;

    int minimalTemp;
    int maximumTemp;

} ThermalConfig;

class Thermal {
    private:
        int intake_fan_pin;
        int exhaust_fan_pin;
        int heater_pin;
        int cooler_pin;

    public:
        Thermal(int intakeFanPin, int exhaustFanPin, int heaterPin, int coolerPin);
        void tick(int temperature);
        void tick(int internalTemp, int externalTemp);

        int intake_power;
        int exhaust_power;

        bool heater_status;
        bool cooler_status;

        ThermalConfig config;
};

#endif
