#if ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#include "Plant.h"

#ifndef UserInterface_h
#define UserInterface_h

#define MODE_MAIN 1
#define MODE_SUB 2
#define MODE_OPTION 3
#define MODE_TRANSITION 4

#define OPTION_NUMBER 1
#define OPTION_TIME 2

#define ACTION_UP 0
#define ACTION_DOWN 1
#define ACTION_OK 2
#define ACTION_BACK 3

typedef struct {
	char name[10];
	long value;
	char suffix[3];
} optionItem;

typedef struct {
	char name[20];
	int targetMode;
	optionItem elements[8];
} menuItem;

class UserInterface {
	private:
		int mode;
		int pointer;
		int lastpointer;

		double easeInOutSine(double t);
		void mainMenuTransition(int source, int destination);

		menuItem[8] main_menu;

		menuItem* current_menu;
	public:
		UserInterface(Plant plant);
		void display();
		void handleAction(int action);
		void drawIcon(menuItem item);
};

#endif