#include <stdio.h>
#include <time.h>
#include <unistd.h>


#include "Plant.h"
#include "Light.h"
#include "Water.h"
#include "Thermal.h"

time_t rawtime;
struct tm * timeinfo;

float currentInTemp = 0;
float currentOutTemp = 0;

Plant plant;

#define LIGHT_PIN 2
#define WATER_PUMP_PIN 3
#define WATER_METER_PIN 4
#define FAN_INTAKE_PIN 5
#define FAN_EXHAUST_PIN 6
#define HEATER_PIN 7
#define COOLER_PIN 8

Light luz(LIGHT_PIN);
Water agua(WATER_PUMP_PIN, WATER_METER_PIN);
Thermal temp(FAN_INTAKE_PIN, FAN_EXHAUST_PIN, HEATER_PIN, COOLER_PIN);


int actualTime = (int)time(NULL) - (3600*3);
float actualTemperature = 25;


void setup() {
	printf("Growino Emulated\n");

	//plant.start = 1525165200;
    plant.start = actualTime;
    plant.currentPeriod = VEGETATIVE;
    plant.vegetativePeriod = 5;
    plant.vegetativeHours = 18;
    plant.floweringPeriod = 30;
    plant.floweringHours = 12;
    plant.periodTransition = 10;

	plant.waterAmount = 350;
	plant.waterInterval = 3 * 24 * 3600; // 3 days;
}

void loop() {
	luz.tick(actualTime, plant);
	agua.tick(actualTime, plant);
    temp.tick(actualTemperature);
	printf("H20: %2.3f ", agua.totalCounter);    

    printf("\r");
}

int main()
{


   setbuf(stdout, NULL);
   actualTime = (int)time(NULL);
   setup();
   char buffer [80];
   
   while (1) {

    /* Simulating time advance (Accelerated)*/ 
	actualTime += 60;

    /* Simulating Overheat */
    actualTemperature += 0.0003f;

    rawtime = (time_t)actualTime;
    timeinfo = gmtime ( &rawtime );
    strftime (buffer,80,"%a %d/%m %H:%M",timeinfo);

    printf("%s ", buffer);

    /* Simulating flow through sensor when watering is active */
    if(agua.pump_state == true) {
        agua.handlePulse();
    }

   	loop();
	usleep(5000);
   }

   return 0;
}	

