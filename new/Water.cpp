#include "Plant.h"
#include "Water.h"
#include "Compat.h"

void Water::tick(long actualTime, Plant &plant) {
    if(actualTime > (long)(plant.waterLast + plant.waterInterval)) {
       if(pump_state == false) {
          plant.waterLast = actualTime;
          plant.changed = true;
          start(); 
       }
    }

    if(pump_state == true && waterCounter >= plant.waterAmount) {
        plant.changed = true;
        end();
    }

    digitalWrite(pump_pin, pump_state);
}

void Water::start() {
    pump_state = true;
    waterCounter = 0;
}

void Water::end() {
    pump_state = false;
}

int Water::getNextWater(Plant &plant) {
    return plant.waterLast + plant.waterInterval;
}

void Water::handlePulse() {
    if((last_pulse + config.pulseDebounce) < millis()) {
        waterCounter += config.pulseAmount;
        totalCounter += config.pulseAmount;
        last_pulse = millis();
    }
}


Water::Water(int pumpPin, int pulsePin) {
  pump_pin = pumpPin;
  pulse_pin = pulsePin;

  pinMode(pumpPin, OUTPUT);

  config.pulseDebounce = 1;
  config.pulseAmount = 0.225f;
};