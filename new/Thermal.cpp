#include <stdio.h>
#include <math.h>
#include "Plant.h"
#include "Thermal.h"
#include "Compat.h"

#define COLD 0
#define HOT 1

Thermal::Thermal(int intakeFanPin, int exhaustFanPin, int heaterPin, int coolerPin) {
    config.targetTemp = 25;
    config.minimalTemp = 15;
    config.maximumTemp = 35;

    intake_fan_pin = intakeFanPin;
    exhaust_fan_pin = exhaustFanPin;
    heater_pin = heaterPin;
    cooler_pin = coolerPin;
    
 }    
void Thermal::tick(int temperature) {
    tick(temperature, temperature);
}

void Thermal::tick(int internalTemp, int externalTemp) {
    bool mode;
    float diff;
    float test;

    int new_intake_power = 0;
    int new_exhaust_power = 0;

    if(internalTemp < config.targetTemp) {
        diff = config.targetTemp - internalTemp;
        mode = HOT;
    } else {
        diff = internalTemp - config.targetTemp;
        mode = COLD;
    }

    float power = (255.f/(((float)config.maximumTemp - (float)config.minimalTemp)/2.f ))*diff; 
    
    new_intake_power = power;
    new_exhaust_power = power;

    if(new_intake_power != intake_power) {
        intake_power = new_intake_power;
        //analogWrite(intake_fan_pin, intake_power); // Full Power
    }
        
    if(new_exhaust_power != exhaust_power) {
        exhaust_power = new_exhaust_power;
        //analogWrite(exhaust_fan_pin, exhaust_power); // Full Power
    }

}