# Growino #

En este repositorio se aloja el código fuente de Growino, el timer inteligente basado en Arduino y ATMEGA328P.

La idea es que el costo de desarrollo sea muy bajo.

### Que hay aca?

*/library* todas las librerías que utiliza Growino, inclusive la librería propietaria *GrowinoPin*

*/growino* la implementación en sketch (.pde)

### Consideraciones

Proximamente